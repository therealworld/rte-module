<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Controller\Admin\ContentMain as OxContentMain;
use OxidEsales\Eshop\Application\Controller\TextEditorHandler as OxTextEditorHandler;
use TheRealWorld\RteModule\Application\Controller\Admin\ContentMain;
use TheRealWorld\RteModule\Application\Controller\TextEditorHandler;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwrte',
    'title' => [
        'de' => 'the-real-world - Text Editor',
        'en' => 'the-real-world - Rich Text Editor',
    ],
    'description' => [
        'de' => 'WYSIWYG-Editor mit Dateiuploader zur Textbearbeitung im Backend.',
        'en' => 'WYSIWYG-Editor with fileupload for word processing in backend.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwrte'),
    'author'    => 'Mario Lorenz (the-real-world.de], CKEditor & Filemanager Developers, + <a href="//ckeditor.com/ckeditor-4/" target="_blank">stable CKEditor4</a> + <a href="//github.com/trippo/ResponsiveFilemanager" target="_blank">Responsive Filemanager</a>',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\RteModule\Core\RteEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\RteModule\Core\RteEvents::onDeactivate',
    ],
    'extend' => [
        // Admin Controller
        OxTextEditorHandler::class => TextEditorHandler::class,
        OxContentMain::class       => ContentMain::class,
    ],
    'blocks' => [
        [
            'template' => 'headitem.tpl',
            'block'    => 'admin_headitem_js',
            'file'     => 'Application/views/blocks/admin_headitem_js.tpl',
        ],
        [
            'template' => 'headitem.tpl',
            'block'    => 'admin_headitem_incjs',
            'file'     => 'Application/views/blocks/admin_headitem_incjs.tpl',
        ],
        [
            'template' => 'content_main.tpl',
            'block'    => 'admin_content_main_editor',
            'file'     => 'Application/views/blocks/admin_content_main_editor.tpl',
        ],
    ],
    'settings' => [
        [
            'group' => 'trwrteconfig',
            'name'  => 'arrLocalCSSFrontendFiles',
            'type'  => 'arr',
            'value' => ['css/styles.min.css'],
        ],
        [
            'group' => 'trwrteconfig',
            'name'  => 'arrExternCSSFrontendFiles',
            'type'  => 'arr',
            'value' => [''],
        ],
        [
            'group' => 'trwrtecontent',
            'name'  => 'boolRTENotForPlainConten',
            'type'  => 'bool',
            'value' => true,
        ],
    ],
];
