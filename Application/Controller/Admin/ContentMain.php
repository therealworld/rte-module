<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\RteModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Content;

/**
 * Admin content manager.
 * There is possibility to change content description, enter page text etc.
 * Admin Menu: Customerinformations -> Content.
 */
class ContentMain extends ContentMain_parent
{
    /** Template Getter is it plain content */
    public function isPlainContent(): bool
    {
        $bResult = false;

        $sOxId = $this->getEditObjectId();

        $oContent = oxNew(Content::class);

        if (isset($sOxId) && $sOxId !== '-1') {
            $oContent->load($sOxId);
            if (stripos($oContent->oxcontents__oxloadid->value, 'plain') !== false) {
                $bResult = true;
            }
        }

        return $bResult;
    }
}
