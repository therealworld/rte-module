[{$smarty.block.parent}]
[{capture assign="sOverloadCopyLongDescJS"}]
    var orig_copyLongDesc = copyLongDesc;
    copyLongDesc = function(sIdent)
    {
        var textVal = null;
        var varName = 'editor_' + sIdent;
        for(var i in CKEDITOR.instances)
        {
            if (CKEDITOR.instances[i].name == varName)
            {
                var editor_data = CKEDITOR.instances[i].getData();
                textVal = cleanupLongDesc(editor_data);
                break;
            }
        }
        if (textVal != null)
        {
            var oTarget = document.getElementsByName('editval[' + sIdent + ']');
            if (oTarget != null && (oField = oTarget.item(0)) != null)
            {
                oField.value = textVal;
            }
        }
        else
        {
            orig_copyLongDesc.apply(this, arguments);
        }
    };
[{/capture}]
[{oxscript add=$sOverloadCopyLongDescJS}]
