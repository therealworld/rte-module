<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\RteModule\Core;

use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;

class RteConfig
{
    /** Paths to of media files */
    protected static array $_aPathConfig = [
        'mediaRtePath'      => 'out/media_rte/',
        'mediaRteThumbPath' => 'out/media_rte_thumbs/',
    ];

    /** Path of vendor script FileManager */
    protected static ?string $_sVendorFileManagerPath = null;

    /** relative Path of vendor script FileManager */
    protected static ?string $_sVendorFileManagerRelativePath = null;

    /** possible shop-path in frontend */
    protected static ?string $_sUrlPathFrontend = null;

    /**
     * get Path Config.
     *
     * @param bool $bCheckPath - create paths if is necessary
     */
    public static function getPathConfig(bool $bCheckPath = false): array
    {
        if ($bCheckPath) {
            foreach (self::$_aPathConfig as $sPath) {
                ToolsFile::checkPathPermissions($sPath);
            }
        }

        return self::$_aPathConfig;
    }

    /** return path of vendor script FileManager */
    public static function getVendorFileManagerPath(): string
    {
        if (is_null(self::$_sVendorFileManagerPath)) {
            $oConfig = Registry::getConfig();

            // Set SSL
            $oConfig->setIsSsl($oConfig->getConfigParam('sSSLShopURL') || $oConfig->getConfigParam('sMallSSLShopURL'));
            self::$_sVendorFileManagerPath = $oConfig->getShopMainUrl() . self::getVendorFileManagerRelativePath();
        }

        return self::$_sVendorFileManagerPath;
    }

    /** return relative path of vendor script FileManager */
    public static function getVendorFileManagerRelativePath(): string
    {
        if (is_null(self::$_sVendorFileManagerRelativePath)) {
            $oConfig = Registry::getConfig();

            // Shop Module Path
            $oModule = oxNew(Module::class);
            $sModulePath = $oConfig->getModulesDir(false) . $oModule->getModulePath('trwrte');

            // Uploader Path for extern Filefinder
            self::$_sVendorFileManagerRelativePath = $sModulePath . '/Vendor/FileManager/';
        }

        return self::$_sVendorFileManagerRelativePath;
    }

    /** return path of vendor script FileManager */
    public static function getUrlPathFrontend(): string
    {
        if (is_null(self::$_sUrlPathFrontend)) {
            $oConfig = Registry::getConfig();

            // Set SSL
            $oConfig->setIsSsl($oConfig->getConfigParam('sSSLShopURL') || $oConfig->getConfigParam('sMallSSLShopURL'));

            // If the shop is not placed in the root url, we determine the url-path for frontend
            $sHost = (
                isset($_SERVER['HTTPS'])
                && $_SERVER['HTTPS']
                && !in_array(Str::getStr()->strtolower($_SERVER['HTTPS']), ['off', 'no']) ?
                'https' :
                'http'
            ) . '://' . $_SERVER['HTTP_HOST'];
            self::$_sUrlPathFrontend = str_replace($sHost, '', $oConfig->getShopMainUrl());
        }

        return self::$_sUrlPathFrontend;
    }
}
