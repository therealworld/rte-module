<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\RteModule\Core;

class RteEvents
{
    /**
     * OXID-Core.
     */
    public static function onActivate()
    {
        // create Media Paths is neccessary
        RteConfig::getPathConfig(true);

        return true;
    }

    /**
     * OXID-Core.
     */
    public static function onDeactivate()
    {
        return true;
    }
}
