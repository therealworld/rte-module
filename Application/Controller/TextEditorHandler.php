<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\RteModule\Application\Controller;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\RteModule\Core\RteConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;

/**
 * Class TextEditorHandler.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\TextEditorHandler
 */
class TextEditorHandler extends TextEditorHandler_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function renderRichTextEditor($width, $height, $objectValue, $fieldName)
    {
        $oConfig = Registry::getConfig();

        if (strpos((string) $width, '%') === false) {
            $width .= 'px';
        }
        if (strpos((string) $height, '%') === false) {
            $height .= 'px';
        }

        // CK Editor has not bind its own upload and browse function for images, etc.
        // We additionally the "FileManager". The "FileManager" requires the real and the URL
        // paths to medias. Here are passed to the CKeditor so he can turn it over
        // to the "FileManager".

        // get absolute Url of FileManager
        $sFileManagerPath = RteConfig::getVendorFileManagerPath();

        // create a Access Key for FileManager (fingerprint of server + Browser)
        $sAccessKey = md5($_SERVER['SERVER_ADDR'] . $_SERVER['SERVER_PORT'] . $_SERVER['HTTP_USER_AGENT']);

        // get relative Media paths
        $aMediaPaths = RteConfig::getPathConfig();
        $aRelativePaths = [];
        foreach ($aMediaPaths as $sKey => $sMediaPath) {
            $aRelativePaths[$sKey] = ToolsFile::getRelativePath(
                RteConfig::getVendorFileManagerRelativePath(),
                $sMediaPath
            );
        }

        // get UploadDir
        $sUploadDir = RteConfig::getUrlPathFrontend() . $aMediaPaths['mediaRtePath'];

        // Paths for Content CSS
        $aContentsCSS = [];
        if ($this->getStyleSheet()) {
            $aContentsCSS[] = $oConfig->getResourceUrl($this->getStyleSheet());
        }
        if ($aLocalCSSFrontendFiles = $oConfig->getConfigParam('arrLocalCSSFrontendFiles')) {
            foreach ($aLocalCSSFrontendFiles as $sLocalCSSFrontendFile) {
                $aContentsCSS[] = $oConfig->getResourceUrl($sLocalCSSFrontendFile);
            }
        }
        if ($aExternCSSFrontendFiles = $oConfig->getConfigParam('arrExternCSSFrontendFiles')) {
            foreach ($aExternCSSFrontendFiles as $sExternCSSFrontendFile) {
                $aContentsCSS[] = $sExternCSSFrontendFile;
            }
        }

        // Session
        $sSession = '';
        if (!$oConfig->getConfigParam('blSessionUseCookies')) {
            $oSession = Registry::getSession();
            $sSession .= '&' . $oSession->getName() . '=' . $oSession->getId();
        }

        $sEditorHtml = "<textarea id='editor_{$fieldName}' " .
            "style='width:{$width}; height:{$height};'>{$objectValue}</textarea>\n";
        $sEditorHtml .= '<script type="text/javascript"><!--' . "\n";
        $sEditorHtml .= "CKEDITOR.replace('editor_{$fieldName}', {\n";
        $sEditorHtml .= "filebrowserBrowseUrl: '" . $sFileManagerPath .
            'dialog.php?type=2&editor=ckeditor&lang=de' . $sSession . '&akey=' . $sAccessKey . "&fldr=',\n";
        $sEditorHtml .= "filebrowserUploadUrl: '" . $sFileManagerPath .
            'dialog.php?type=2&editor=ckeditor&lang=de' . $sSession . '&akey=' . $sAccessKey . "&fldr=',\n";
        $sEditorHtml .= "language: 'de',\n";
        $sEditorHtml .= "removeDialogTabs: 'link:upload;image:Upload',\n";
        $sEditorHtml .= "contentsCss: ['" . implode("','", $aContentsCSS) . "']\n";
        if (method_exists($this, 'getCustomCKEditorConfigFile')) {
            $sEditorHtml .= ", customConfig: '" . $this->getCustomCKEditorConfigFile() . "'\n";
        }
        $sEditorHtml .= "});\n";
        $sEditorHtml .= "\n--></script>\n";

        $_SESSION['OxidRTEConfig'] = [
            'base_url'         => '',
            'filemanager_path' => $sFileManagerPath,
            'upload_dir'       => $sUploadDir,
            'current_path'     => $aRelativePaths['mediaRtePath'],
            'thumbs_base_path' => $aRelativePaths['mediaRteThumbPath'],
        ];

        return $sEditorHtml;
    }
}
