<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwrteconfig'  => 'Configuration',
    'SHOP_MODULE_GROUP_trwrtecontent' => 'CMS Pages',

    'SHOP_MODULE_arrLocalCSSFrontendFiles'  => 'Names of locale Frontend-CSS-Files. Each File per Line. (e.g.: "<b>out/yourtheme/src/<b>css/styles.min.css</b>")',
    'SHOP_MODULE_arrExternCSSFrontendFiles' => 'Names of external Frontend-CSS-Files. Each File per Line. (e.g.: "<b>https://fonts.googleapis.com/css?family=EB+Garamond</b>")',
    'SHOP_MODULE_boolRTENotForPlainConten'  => 'dont use RTE-Editor for Plain-Content',
];
