# Update CKEditor

## Download

### Variante 1
go to ["customize CKEditor"](https://ckeditor.com/cke4/builder)
* click "Online Builder"
* during customize choose preset "Standard"
* select the following additional plugins:
    * Advanced Tab for Dialogs
    * Content Templates
    * Youtube Plugin
    * Show Protected
    * Layout Manager
* select a skin, recommended: Moono-Lisa
* select a editors Language, recommended: English, German
* Download as Optimized

### Variante 2
go to ["customize CKEditor"](https://ckeditor.com/cke4/builder)
* klick on "upload build-config.js" (Vendor/CKEditor/build-config.js)

## Prepare CKEDitor Vendor-Folder (preparation)
* rename old Folder "Vendor/CKEditor" to "Vendor/CKEditor_old"
* extract from zip the Folder "ckeditor" to "Vendor/CKEditor"
* delete the unnecessary folder "Vendor/CKEditor/samples"
* check now the editor
* if everything works as usual, then delete "Vendor/CKEditor_old"

# update "responsive filemanager"

## Download
Download "Zip package" from [github.com/trippo](https://github.com/trippo/ResponsiveFilemanager/releases/)

## Prepare FileManager Vendor-Folder (preparation)
* rename old Folder "Vendor/FileManager" to "Vendor/FileManager_old"
* extract from zip the Folder "filemanager???" to "modules/ckeditor/Vendor/FileManager"
* rename the file "Vendor/FileManager/config/config.php" in "Vendor/FileManager/config/config_default.php"
* copy the file "Vendor/FileManager_old/config/config.php" to "Vendor/FileManager/config/config.php"

## compare old and new FileManager Config
compare the file "Vendor/FileManager/config/config.php" vs. "Vendor/FileManager/config/config_default.php"
in the beginning of the file should stay this:
```
  ini_set('session.use_cookies', 0);
  ini_set('session.name', 'sid');
  ini_set('session.use_trans_sid', 0);
  if ($sSessionId = ($_COOKIE['admin_sid'] ? $_COOKIE['admin_sid'] : ($_GET['admin_sid'] ? $_GET['admin_sid'] : null)))
  {
    session_id($sSessionId);
    session_start();
  }
  // create a Access Key for FileManager (fingerprint of server + Browser)
  $sOxidAccessKey = md5($_SERVER['SERVER_ADDR'] . $_SERVER['SERVER_PORT'] . $_SERVER['HTTP_USER_AGENT']);

  check if config-paths come from session:

  /*
  |--------------------------------------------------------------------------
  | DON'T TOUCH (base url (only domain) of site).
  |--------------------------------------------------------------------------
  |
  | without final / (DON'T TOUCH)
  | e.g. 'base_url' => ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && ! in_array(strtolower($_SERVER['HTTPS']), array( 'off', 'no' ))) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'],
  |
  */
  'base_url' => $_SESSION['OxidRTEConfig']['base_url'],
  /*
  |--------------------------------------------------------------------------
  | path from base_url to base of upload folder
  |--------------------------------------------------------------------------
  |
  | with start and final /
  | e.g. 'upload_dir' => '/source/',
  |
  */
  'upload_dir' => $_SESSION['OxidRTEConfig']['upload_dir'],
  /*
  |--------------------------------------------------------------------------
  | relative path from filemanager folder to upload folder
  |--------------------------------------------------------------------------
  |
  | with final /
  | e.g. 'current_path' => '../source/',
  |
  */
  'current_path' => $_SESSION['OxidRTEConfig']['current_path'],

  /*
  |--------------------------------------------------------------------------
  | relative path from filemanager folder to thumbs folder
  |--------------------------------------------------------------------------
  |
  | with final /
  | DO NOT put inside upload folder
  | e.g. 'thumbs_base_path' => '../thumbs/',
  |
  */
  'thumbs_base_path' => $_SESSION['OxidRTEConfig']['thumbs_base_path'],

  add a md5 from upload_dir as accessKey. Look for the var 'access_keys'

  'access_keys' => array(
    $sOxidAccessKey
  ),
```

## Prepare FileManager Vendor-Folder (follow up)
* check now the filefind-function in ckeditor
* if everything works as usual, then delete "Vendor/FileManager_old"