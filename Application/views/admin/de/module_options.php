<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwrteconfig'  => 'Konfiguration',
    'SHOP_MODULE_GROUP_trwrtecontent' => 'CMS-Seiten',

    'SHOP_MODULE_arrLocalCSSFrontendFiles'  => 'Namen der lokalen Frontend-CSS-Dateien. Je Zeile eine Datei. (z.B.: "<b>out/yourtheme/src/<b>css/styles.min.css</b>")',
    'SHOP_MODULE_arrExternCSSFrontendFiles' => 'Namen der externen Frontend-CSS-Files. Je Zeile eine Datei. (e.g.: "<b>https://fonts.googleapis.com/css?family=EB+Garamond</b>")',
    'SHOP_MODULE_boolRTENotForPlainConten'  => 'RTE-Editor nicht für Plain-Content nutzen',
];
